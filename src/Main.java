import java.text.DecimalFormat;
import java.util.concurrent.TimeUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.*;

import com.jaunt.*;

public class Main {

    public static UserAgent ua = new UserAgent();

    public static ArrayList<String> originalColleges = new ArrayList<String>(Arrays.asList( // colleges based on USNews top national universities
            "01 Princeton University",
            "02 Harvard University",
            "03t University of Chicago",
            "03t Yale University",
            "05t Columbia University in the City of New York",
            "05t Massachusetts Institute of Technology",
            "05t Stanford University",
            "08 University of Pennsylvania",
            "09 Duke University",
            "10 California Institute of Technology",
            "11t Dartmouth College",
            "11t Johns Hopkins University",
            "11t Northwestern University",
            "14t Brown University",
            "14t Cornell University",
            "14t Rice University",
            "14t Vanderbilt University",
            "18t University of Notre Dame",
            "18t Washington University in St. Louis",
            "20 Georgetown University",
            "21t Emory University",
            "21t University of California, Berkeley",
            "21t University of California, Los Angeles",
            "21t University of Southern California",
            "25t Carnegie Mellon University",
            "25t University of Virginia",
            "27 Wake Forest University",
            "28 University of Michigan - Ann Arbor",
            "29 Tufts University",
            "30t New York University",
            "30t University of North Carolina at Chapel Hill",
            "32t Boston College",
            "32t College of William and Mary",
            "34t Brandeis University",
            "34t Georgia Institute of Technology",
            "34t University of Rochester",
            "37t Boston University",
            "37t Case Western Reserve University",
            "37t University of California, Santa Barbara",
            "40t Northeastern University",
            "40t Tulane University",
            "42t Rensselaer Polytechnic Institute",
            "42t University of California, Irvine",
            "42t University of California, San Diego",
            "42t University of Florida",
            "46t Lehigh University",
            "46t Pepperdine University",
            "46t University of California, Davis",
            "46t University of Miami",
            "46t University of Wisconsin - Madison",
            "46t Villanova University"
    ));

    public static ArrayList<String> formattedColleges = new ArrayList<String>();

    public static void main(String[] args) throws ResponseException, NotFound, InterruptedException {

        System.out.println(
                "<!DOCTYPE html>" +
                        "<html>" +
                        "<body>" +
                        "<table style=\"width:100%\">" +
                        "<tr>" +
                            "<th>College1</th>"
        );

        for (String e : originalColleges) {
            System.out.println("<th>" + e + "</th>");
            e = e.replaceAll("\\d+t?\\s", "").replaceAll("\\s+","+");
            formattedColleges.add(e);
        }

        System.out.println("</tr>");

        for (int i = 0; i < originalColleges.size(); i++) {
            System.out.println("<tr>");
            System.out.println("<td>" + originalColleges.get(i) + "</td>");
            for (int j = 0; j < originalColleges.size(); j++) {
                System.out.println("<td>");
                if (i == j) {
                    System.out.println("//");
                    continue;
                }
                try {
                    ua.visit("http://www.parchment.com/c/college/tools/college-cross-admit-comparison.php?compare=" +
                            formattedColleges.get(i) + "&with=" + formattedColleges.get(j));

                    /*
                    System.out.println(originalColleges.get(i).toUpperCase() + " vs " + originalColleges.get(j).toUpperCase() + ":");
                    String favorabilityStr = ua.doc.findEvery("<span>%").getFirst("<span>").innerText();
                    int favorability = Integer.parseInt(favorabilityStr.substring(0, favorabilityStr.length() - 1));
                    System.out.println(favorability + "% vs " + (100 - favorability) + "%");
                    */

                    String favorabilityStr = ua.doc.findEvery("<span>%").getFirst("<span>").innerText();
                    int favorability = Integer.parseInt(favorabilityStr.substring(0, favorabilityStr.length() - 1));
                    System.out.println(favorability + "%");
                    System.out.println("</td>");
                }
                catch (Exception e) {
                    System.out.println("Error! Continuing...");
                    Thread.sleep(2000);
                    System.out.println("</td>");
                    continue;
                }
            }
            System.out.println("</tr>");
            // System.out.println("———————————————————————————————————————————————");
        }
        System.out.println("</table></body></html>");
    }
}
